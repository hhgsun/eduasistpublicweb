﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace EduAsistWeb.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult Iletisim()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Iletisim(
             string firstname
            ,string lastname
            ,string email
            ,string tel
            ,string kurumadi
            ,string mesaj
            )
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("asistan@eduasist.com");
                mail.To.Add("ayalcin@eduasist.com");
                mail.To.Add("iseki@eduasist.com");
                mail.To.Add("scelik@eduasist.com");
                mail.To.Add("smercan@eduasist.com");
                mail.Subject = "EduAsist İletişim Fromu Dolduruldu!";
                mail.IsBodyHtml = true;

                mesaj = "<h2>Web Sayfası İletişim Formu</h2>"
                        + "<h3>" + firstname + " " + lastname + "</h3>"
                        + "<h4>" + tel + ", " + email + "</h4>"
                        + "<h4>" + kurumadi + "</h4>"
                        + "<p>" + mesaj + "</p>";

                mail.Body = mesaj;
                SmtpClient client = new SmtpClient("smtp.yandex.ru", 587);
                client.Credentials = new NetworkCredential("asistan@eduasist.com", "F:=#9])Y]%!cRv_X");
                client.EnableSsl = true;
                client.Send(mail);

                ViewBag.post = true;
            }
            catch (Exception)
            {
                ViewBag.post = false;
            }

            return View();
        }
    }
}