﻿
// LOAD EFFECTS
function transitionElementsPageOnLoad(){
  var navMenuLinks = document.getElementsByClassName('nav-link');
  if(navMenuLinks.length > 0) {
    for (let i = 0; i < navMenuLinks.length; i++) {
      const linkItem = navMenuLinks.item(i);
      linkItem.style.opacity = 1;
    }
  }

  var heroTitle = document.getElementsByClassName('hero-title').item(0);
  if(heroTitle){
    heroTitle.style.opacity = 1;
  }

  var heroSubTitle = document.getElementsByClassName('hero-subtitle').item(0);
  if(heroSubTitle){
    heroSubTitle.style.opacity = 1;
  }

  var heroSpecifications = document.getElementsByClassName('hero-specifications').item(0);
  if(heroSpecifications){
    heroSpecifications.style.opacity = 1;
  }

  var heroLazyBg = document.getElementsByClassName('hero-image-bg').item(0);
  if(heroLazyBg){
    setTimeout(() => {
        heroLazyBg.style.backgroundPositionX = 'right';
    }, 200);
  }

    var heroImages = document.getElementsByClassName("hero-image");
    if (heroImages.length > 1) {
        setTimeout(() => {
            heroImagesSlider(heroImages);
        }, 200);
    } else if (heroImages.length == 1) {
        heroImages[0].style.opacity = 1;
    }
    
}
window.onload = transitionElementsPageOnLoad();


// HERO IMAGES SLIDER
function heroImagesSlider(images) {
    for (let i = 0; i < images.length; i++) {            
        setTimeout(() => {
            if (i - 1 < 0) {
                images[images.length - 1].style.opacity = 0;
            } else {
                images[i - 1].style.opacity = 0;
            }
            images[i].style.opacity = 1;
            if (i == images.length - 1) {
                setTimeout(() => heroImagesSlider(images), 10000);
            }
        }, i * 10000);
    }
}


// HEADER STICKY
var header,stickyTop, pageContentMenu;
function loadValues(){
  header = document.getElementById("header");
  stickyTop = document.getElementById('hero').offsetHeight;
  pageContentMenu = document.getElementById('page-content-menu');
  pageContentMenuStickyTop = stickyTop * 2;
  stickyHeader();
}
function stickyHeader(){
  if(window.pageYOffset > stickyTop - 72) {
    header.classList.add('sticky');
  } else {
    header.classList.remove('sticky');
  }
  if(pageContentMenu){
    if(window.pageYOffset > pageContentMenuStickyTop) {
      pageContentMenu.classList.add('visible');
    } else {
      pageContentMenu.classList.remove('visible');
    }
  }
}
loadValues();
window.onscroll = stickyHeader;


// SCROLLING SMOOTH
$(document).ready(function(){
  $("a.more-link").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 500, function(){
        window.location.hash = hash;
      });
    }
  });
});


// LOAD COLLAPSE
$(document).ready(function () {

    $(".btn-collapse").on('click', function (event) {
        var collapseArea = $(this).parent().get(0);
        $(collapseArea).toggleClass('open');
    });

});