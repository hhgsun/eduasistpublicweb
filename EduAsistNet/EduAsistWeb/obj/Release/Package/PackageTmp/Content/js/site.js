﻿const hero = document.querySelector("#hero");

// LOAD EFFECTS
function transitionElementsPageOnLoad(){
  var navMenuLinks = document.getElementsByClassName('nav-link');
  if(navMenuLinks.length > 0) {
    for (let i = 0; i < navMenuLinks.length; i++) {
      const linkItem = navMenuLinks.item(i);
      linkItem.style.opacity = '1';
    }
  }

  var heroTitle = document.getElementsByClassName('hero-title').item(0);
  if(heroTitle){
    heroTitle.style.opacity = 1;
  }

  var heroSubTitle = document.getElementsByClassName('hero-subtitle').item(0);
  if(heroSubTitle){
    heroSubTitle.style.opacity = 1;
  }

  var heroSpecifications = document.getElementsByClassName('hero-specifications').item(0);
  if(heroSpecifications){
    heroSpecifications.style.opacity = 1;
  }

  var heroLazyBg = document.getElementsByClassName('hero-bg-lazy').item(0);
  if(heroLazyBg){
    heroLazyBg.style.backgroundPositionX = 'right';
    setTimeout(() => {
      /* heroLazyBg.style.transition = 'none'; */
      document.addEventListener("mousemove", parallax);
    }, 200);
  }
  
}
window.onload = transitionElementsPageOnLoad();


// HEADER STICKY
var header,stickyTop, pageContentMenu;
function loadValues(){
  header = document.getElementById("header");
  stickyTop = document.getElementById('hero').offsetHeight;
  pageContentMenu = document.getElementById('page-content-menu');
  pageContentMenuStickyTop = stickyTop * 2;
  stickyHeader();
}
function stickyHeader(){
  if(window.pageYOffset > stickyTop - 72) {
    header.classList.add('sticky');
  } else {
    header.classList.remove('sticky');
  }
  if(pageContentMenu){
    if(window.pageYOffset > pageContentMenuStickyTop) {
      pageContentMenu.classList.add('visible');
    } else {
      pageContentMenu.classList.remove('visible');
    }
  }
}
loadValues();
window.onscroll = stickyHeader;


// HERO BACKGROUND MOUSE EFFECT
function parallax(e) {
  let _w = window.innerWidth/1;
  let _h = window.innerHeight * 3;
  let _mouseX = e.clientX;
  let _mouseY = e.clientY;
  let _depth1 = `${100 - (_mouseX - _w) * 0.005}% ${100 - (_mouseY - _h) * 0.01}%`;
  let _depth2 = `${100 - (_mouseX - _w) * 0.005}% ${100 - (_mouseY - _h) * 0.01}%`;
  let x = `${_depth2}, ${_depth1}`;
  hero.style.backgroundPosition = x;
}


// SCROLLING SMOOTH
$(document).ready(function(){
  $("a.more-link").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 500, function(){
        window.location.hash = hash;
      });
    }
  });
});