
// LOAD EFFECTS
function transitionElementsPageOnLoad(){
  var navMenuLinks = document.getElementsByClassName('nav-link');
  if(navMenuLinks.length > 0) {
    for (let i = 0; i < navMenuLinks.length; i++) {
      const linkItem = navMenuLinks.item(i);
      linkItem.style.opacity = '1';
    }
  }

  var heroTitle = document.getElementsByClassName('hero-title').item(0);
  if(heroTitle){
    heroTitle.style.opacity = 1;
  }

  var heroSubTitle = document.getElementsByClassName('hero-subtitle').item(0);
  if(heroSubTitle){
    heroSubTitle.style.opacity = 1;
  }

  var heroBg1 = document.getElementsByClassName('hero-bg-1').item(0);
  if(heroBg1){
    setTimeout(() => {
      heroBg1.style.backgroundPositionX = 'right';
    }, 500);
  }

  var heroBg2 = document.getElementsByClassName('hero-bg-2').item(0);
  if(heroBg2){
    setTimeout(() => {
      heroBg2.style.backgroundPositionY = '60vh';
    }, 500);
  }
}
window.onload = transitionElementsPageOnLoad();


// HEADER STICKY
var header,stickyTop;
function loadValues(){
  header = document.getElementById("header");
  stickyTop = document.getElementById('hero').offsetHeight;
}
function stickyHeader(){
  if(window.pageYOffset > stickyTop - 72) {
    header.classList.add('sticky');
  } else {
    header.classList.remove('sticky');
  }
}
loadValues();
window.onscroll = stickyHeader;




